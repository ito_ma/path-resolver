<?php
class LibraryNotFoundException extends RuntimeException{}

class Lib{
	const Printer = "Printer";
	const Shower = "Shower";
}

class Resolver{

	private $libroot = "./lib"; // /full/path/to/lib/

	/**
	 * set library path.
	 */
	private $libhash = array(
		Lib::Printer => "printer.class.php"
		);

	// singleton 
	private static $instance = null;
	private function __construct(){}
	public static function getInstance()
	{
		if(is_null(self::$instance))
		{
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * require_once 
	 *
	 * @param string $libname
	 * @throws LibraryNotFoundException
	 */ 
	public function import($libname)
	{
		if(array_key_exists($libname, $this->libhash))
		{
			require_once($this->libroot.DIRECTORY_SEPARATOR.$this->libhash[$libname]);
		}
		else
		{
			throw new LibraryNotFoundException(sprintf("I don't know this library-name ['%s']",$libname));
		}
	}
}
