path-resolver
==============

resolverの中にライブラリパスをすべて押し込んでしまえば、後はresolverまでのパスを解決するだけ。resolver.phpに定義しているLibクラス(IDE名前補完用)に定義したライブラリ名で呼び出せる。

autoloadと組み合わせれば、resolverまでのパスを解決する必要はないかもしれない。

usage
------

```php
/path/to/deep/forest/deep/sealand/main.php
require_once("/path/to/resolver.php");
$loader = Resolver::getInstance();
$loader->import(Lib::Printer);

$printer = new Printer();
$printer->hoge("foobar\n");
```

深い階層からでもresolverまでのパスを解決できればどんな位置にあるライブラリも気にせず名前で呼び出せる。auto_loadが使えないときや古い環境でも動かせると思う。